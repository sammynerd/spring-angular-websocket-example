import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {SocketService} from "./services/socket.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  greeting: string = "";
  ngOnInit() {
    this.title.setTitle('Angular Spring Websocket');
  }

  constructor(private title: Title,
              private sockjsService: SocketService) {
    this.getProject()
    this.sendMessages()
  }

  sendMessages() {
    setInterval(() => {
      this.sockjsService.getPins("/projects/1/pins")
      // this.sockjsService.send("/greet", "Hi Sam")
    }, 1000)
  }

  getProject() {
    this.sockjsService.connect("/topic/pins").subscribe((result: any) => {
      console.log("Result: "+ result);
      this.greeting = result
      console.log("Result: "+ result);
    }, error => {
      console.log(error)
    })
  }
}
