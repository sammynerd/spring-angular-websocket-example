import {Injectable} from '@angular/core'
import * as SockJS from "sockjs-client";
import * as Stomp from "stompjs";
import {Observable} from 'rxjs/internal/Observable';

// const url = '/proxy'
const token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJTN29aeDJ3ck45ek9kNFN1UHkxSGh6cmFXSUF3aEF3UzF5Q1RhQl9fR1ljIn0.eyJleHAiOjE2MjY4MjI2MzIsImlhdCI6MTYyNjc3OTQzMiwianRpIjoiMDM0N2YwNTgtNjU5Yi00OGE5LTg2ZjAtMDNlZjg3NzFlYzIwIiwiaXNzIjoiaHR0cHM6Ly9hdXRoLmJldGhyZWFkLTE5Mi4zNC41OC4xNzMubmlwLmlvL2F1dGgvcmVhbG1zL3N0YWdpbmciLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiNDUwMDU0OTMtNGY0ZC00Njk3LWFjZDEtNjEyYjJmYjU3NWQwIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoid2ViLWNsaWVudCIsInNlc3Npb25fc3RhdGUiOiIyZjQxMjVhMi1iN2Y3LTQwMWItOWVmYy1iMzM3ZmVkYTE2NGEiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiYWRtaW4iLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsIm9yZ2FuaXphdGlvbklkIjoxLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJTYW11ZWwgTSIsInByZWZlcnJlZF91c2VybmFtZSI6Im1idWd1YW5qYW5lMjAwNUBnbWFpbC5jb20iLCJnaXZlbl9uYW1lIjoiU2FtdWVsIiwiZmFtaWx5X25hbWUiOiJNIiwidXNlcklkIjoyLCJlbWFpbCI6Im1idWd1YW5qYW5lMjAwNUBnbWFpbC5jb20ifQ.FiC2Cr0H8jT4PHp8rMDC1XwFsABrEN-4HIR2hhBuGLrt1nsFVCx9dlQBmYxz7xgq2JvtvDfRlyee00Gv0h_MB4fQSsIY_ogTSnSTarMF1Zg5xnKl99MwaiiNGt996H2NITDs0zwu7I5l2x5Lzty0I72KEaqOIWn-vfLx5I_9bmi_kHpu2mZohiZQVFc6l9CcTJQG7kAw4NxKfLHKM-yLzvpU3dLa6MoP7IzZkP1b4Y2askUGzwuo0Jwa6mnxahGa5WN_ZbXi-G5FhQtgv1Z9VnX6ks9x3hv1mWH6AxLlN3qF3uaYRTRrE6hEB2qdUUkRpUUIu4qfg1qYPfqrc100AQ';
const url = 'https://api.bethread-192.34.58.173.nip.io/accounts/websocket/?access_token='+token;
// const url = 'https://api.bethread-192.34.58.173.nip.io/accounts/websocket';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  client: any;
  authHeader: any;

  constructor() {
    let ws = new SockJS(url);
    this.client = Stomp.over(ws);
    this.authHeader = {'X-Authorization': `Bearer ${token}`};
  }

  connect<T>(path: string): Observable<T> {
    return Observable.create((observer: any) => {
      this.client.connect(this.authHeader, () => {
        console.log("Connected")
        this.client.subscribe(path, (message: any) => {
          if (message.body) {
            observer.next(message.body)
          } else {
            console.log("No message body")
          }
        });
      }, (error: any) => {
        observer.error(error)
      });
    })

  }

  send(path: string, payload: string) {
    this.client.send(path, {}, payload)
  }


  getPins(path: string) {
    this.client.send(path)
  }
}


