package com.example.stomp;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfig {

    @Value("${kafka.input.topic}")
    private String kafkaInputTopic;

    @Value("${kafka.output.topic}")
    private String kafkaOutputTopic;

    @Bean
    public NewTopic inputTopic() {

        return new NewTopic(kafkaInputTopic, 1, (short) 1);
    }

    @Bean
    public NewTopic outputTopic() {

        return new NewTopic(kafkaOutputTopic, 1, (short) 1);
    }

}
