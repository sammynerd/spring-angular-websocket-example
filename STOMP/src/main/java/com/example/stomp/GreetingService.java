package com.example.stomp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.UUID;

@Service
public class GreetingService {
    @Value("${kafka.input.topic}")
    private String kafkaInputTopic;

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public GreetingService(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public String greet(Greeting greeting) {
        Calendar c = Calendar.getInstance();

        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        StringBuilder sb = new StringBuilder();

        String message;

        if (timeOfDay < 12) {
            message = greeting.getName();
        } else if (timeOfDay < 16) {
            message = greeting.getName();
        } else if (timeOfDay < 21) {
            message = greeting.getName();
        } else {
            message = greeting.getName();
        }

        sb.append(message).append(" - ").append(generateString());
//        kafkaTemplate.send(kafkaInputTopic, sb.toString());
        return sb.toString();
    }

    public String getGreeting(){
        StringBuilder sb = new StringBuilder();
        sb.append("Hi").append(" - ").append(generateString());
        return sb.toString();
    }

    private String generateString() {
        return UUID.randomUUID().toString();
    }

}
