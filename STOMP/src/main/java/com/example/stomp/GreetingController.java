package com.example.stomp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class GreetingController {

    @Autowired
    private GreetingService greetingService;

    @MessageMapping("/greet")
    @SendTo("/topic/projects/1")
    public String greeting(@Payload String greeting) {
        return greetingService.greet(new Greeting(null, greeting));
    }


    @MessageMapping("/topic/projects/1")
    @SendTo("/topic/projects/1")
    public String getGreeting() throws Exception {
        return greetingService.getGreeting();
    }
}
